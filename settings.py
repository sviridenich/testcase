
PROJECT_PATH = "/home/sviridenich/PycharmProjects/testcase"
FULL_TRAIN_SET = PROJECT_PATH + "/data/original/train.csv DS.csv"
SUBMISSION_SET = PROJECT_PATH + "/data/original/test.csvDS.csv"
TRANSFORMED_TRAIN_SET = PROJECT_PATH + "/data/transformed/train.csv"
TRANSFORMED_SUBMISSION_SET = PROJECT_PATH + "/data/transformed/submission.csv"
CHARTS_DIR = PROJECT_PATH + "/charts"

TRAIN_SUBSET = PROJECT_PATH + "/data/splitted/train.csv"
VALIDATION_SUBSET = PROJECT_PATH + "/data/splitted/validation.csv"
TEST_SUBSET = PROJECT_PATH + "/data/splitted/test.csv"
SUBMISSION_SUBSET = PROJECT_PATH + "/data/splitted/submission.csv"

PERFORMANCE_REPORT = PROJECT_PATH + "/report/performance.csv"
MODEL_SERIALIZED = PROJECT_PATH + "/outputs/model.h5"
SUBMISSION = PROJECT_PATH + "/outputs/predictions.csv"