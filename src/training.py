from keras import models, layers

from settings import PERFORMANCE_REPORT, MODEL_SERIALIZED, SUBMISSION
from src.loader import load_data, NUMBER_OF_FEATURES
import tensorflow as tf
from sklearn.metrics import roc_auc_score
import pandas as pd

def aucroc(y_true, y_pred):
    return tf.py_function(roc_auc_score, (y_true, y_pred), tf.double)


def get_single_layer_model(number_of_hidden_units):
    #network architecture
    network = models.Sequential()

    #compilation step
    network.add(layers.Dense(number_of_hidden_units, kernel_initializer = "uniform", activation="relu", input_shape=(NUMBER_OF_FEATURES,)))
    network.add(layers.Dense(1, kernel_initializer = "uniform", activation='sigmoid'))
    network.compile(optimizer='adam', loss='binary_crossentropy', metrics=[aucroc])
    return network


def grid_search(neurons_range, epochs_range):
    with open(PERFORMANCE_REPORT, 'w') as f:
        f.write("neurons,epoc,auc\n")
        for neurons_number in neurons_range:
            for epochs_numbder in epochs_range:
                network = get_single_layer_model(neurons_number)

                network.fit(train_data, train_labels, class_weight=[6, 100], epochs=epochs_numbder, batch_size=1000)

                print("\ncalculatting area under curve on validation dataset\n")
                validation_loss, validation_auc = network.evaluate(test_data, test_labels, batch_size=1000)
                print('auc:', validation_auc)
                f.write("{0},{1},{2}\n".format(neurons_number,epochs_numbder, validation_auc))


if __name__=="__main__":

    train_ds, validation_ds, test_ds, submission_ds = load_data()
    train_data, train_labels = train_ds
    validation_data, validation_labels = validation_ds
    test_data, test_labels = test_ds
    submisssion_data, _ = submission_ds

    number_of_neurons = 32
    number_of_epochs = 1024

    network = get_single_layer_model(number_of_neurons)
    network.fit(train_data, train_labels, class_weight=[6, 100], epochs=number_of_epochs, batch_size=1000)

    tets_loss, test_auc = network.evaluate(test_data, test_labels, batch_size=1000)
    print('test_auc:', test_auc)

    validation_loss, validation_auc = network.evaluate(validation_data, validation_labels, batch_size=1000)
    print('validation_auc:', validation_auc)

    network.save(MODEL_SERIALIZED)

    predictions = network.predict(submisssion_data)
    res = pd.DataFrame(predictions)
    res.to_csv(SUBMISSION)


