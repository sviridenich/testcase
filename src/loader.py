from settings import *
import pandas as pd
import numpy as np

FEATURES_TO_INCLUDE = ['feature1', 'age', 'feature2', 'feature3',
                       'feature5', 'feature6', 'feature7', 'feature8',
                       'dummy4', 'feature4_new', 'dummy9', 'feature9_new']

SCALING_FACTORS = {
    'feature1': 1,
    'feature2': 5,
    'feature3': 25000,
    'feature5': 25,
    'feature6': 5,
    'feature7': 5,
    'feature8': 5,
    'feature4_new': 25000,
    'feature9_new': 5
}

NUMBER_OF_FEATURES = len(FEATURES_TO_INCLUDE)

def load_subset(filepath):
    df = pd.read_csv(filepath)
    temp = df
    for col in FEATURES_TO_INCLUDE:
        scaling_factor = SCALING_FACTORS.get(col, 1)
        temp[col] = df[col] / scaling_factor
    data = temp[FEATURES_TO_INCLUDE].values
    try:
        labels = df['target'].values
    except KeyError:
        labels = None
    return data, labels

def load_data():
    train = load_subset(TRAIN_SUBSET)
    validation = load_subset(VALIDATION_SUBSET)
    test = load_subset(TEST_SUBSET)
    submission = load_subset(SUBMISSION_SUBSET)
    return train, validation, test, submission


if __name__=="__main__":
    data, labels = load_subset(TRAIN_SUBSET)

    print(data)
    print(labels)

    data, labels = load_subset(SUBMISSION_SUBSET)

    print(data)
    print(labels)
