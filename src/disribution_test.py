from settings import FULL_TRAIN_SET, SUBMISSION_SET
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats


if __name__=="__main__":
    train_set = pd.read_csv(FULL_TRAIN_SET)
    test_set = pd.read_csv(SUBMISSION_SET)

    columns = train_set.columns
    columns_to_exclude = "target"
    for col in columns:
        if col not in columns_to_exclude:
            print("checking the distribtuion similarity between dataset for variable - {0}".format(col))
            res = stats.ks_2samp(train_set[col], test_set[col])
            print(res)

    # inference all features have the same distribution between train and dataset
    # therefore when we will build the model and calculate its performeance for validation set
    # we can be pretty sure that the model will show the same performance on submission dataset