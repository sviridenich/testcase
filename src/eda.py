from settings import FULL_TRAIN_SET, SUBMISSION_SET, CHARTS_DIR
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def print_agg(data_frame):
    for col in data_frame.columns:
        agg = data_frame[col].agg(['sum', 'count', 'mean', 'std'])
        print(agg)
        print('--------------------')


def print_basic_info(data_frame):
    print(data_frame)
    print("#################")
    print(data_frame.head())
    print("#################")
    print(data_frame.shape)
    print("#################")
    print_agg(data_frame)
    print("#################")


def print_histograms(input_file_path, col2exclude, outpattern):
    df = pd.read_csv(input_file_path)
    df = df.replace(r'^\s*$', np.nan, regex=True)
    for col in df.columns:
        print("building the hist for {0}".format(col))
        if col not in col2exclude:
            hist = plt.hist(df[col], bins=20)
            # plt.show(hist)
            filename = outpattern.format(CHARTS_DIR, "hists", col)
            plt.savefig(filename)
            plt.close()


def print_boxplots(input_file_path, col2include, outpattern):
    df = pd.read_csv(input_file_path)
    df = df.replace(r'^\s*$', np.nan, regex=True)
    for col in col2include:
        plt.boxplot(df[col])
        filename = outpattern.format(CHARTS_DIR, "boxes", col)
        plt.savefig(filename)
        plt.close()

if __name__ == "__main__":

    ### basic information about datasets
    for file_path in [FULL_TRAIN_SET, SUBMISSION_SET]:
        print("reading the data from {0}".format(file_path))
        df = pd.read_csv(file_path)
        df = df.replace(r'^\s*$', np.nan, regex=True)
        print_basic_info(df)

    # inference; some columns contains missing values

    ### visualising the data
    columns_with_nans = ["feature4", "feature9", "target"]
    print_histograms(FULL_TRAIN_SET, columns_with_nans, "{0}\\{1}\\{2}train_.png")
    print_histograms(SUBMISSION_SET, columns_with_nans, "{0}\\{1}\\{2}test_.png")

    # inference: all feature except maybe age & feature 5 has non normal distribution
    #  feature1 lies withing [0: 1] interval
    #  feature2 has only several values, therefore can be treated either as categorical or numerical
    # feature3 has very skewed distirbution with long tail
    # feature5 potentiallly can be treated as normally distributed
    # feature6 has only few values, therefore can treated either as categorical or numerical
    # feature7 same as above
    # feature8 same as above

    col2include = ["feature1", "age", "feature3", "feature5"]

    print_boxplots(FULL_TRAIN_SET, col2include, "{0}\\{1}\\{2}train_.png")
    print_boxplots(FULL_TRAIN_SET, col2include, "{0}\\{1}\\{2}test_.png")

    # inference;
    # feature3 has significant number of outliers and heavy tail
    # features5 has few outliers