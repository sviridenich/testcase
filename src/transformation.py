from settings import *
import pandas as pd
import numpy as np


def transform_dataset(input_file, output_file):
    df = pd.read_csv(input_file)
    df = df.replace(r'^\s*$', np.nan, regex=True)

    df["dummy4"] = 1*pd.isnull(df["feature4"])
    df["feature4_new"] = df["feature4"]
    df["feature4_new"].fillna(0, inplace = True)

    df["dummy9"] = 1*pd.isnull(df["feature9"])
    df["feature9_new"] = df["feature9"]
    df["feature9_new"].fillna(0, inplace = True)

    df.to_csv(output_file)


if __name__ == "__main__":

    transform_dataset(FULL_TRAIN_SET, TRANSFORMED_TRAIN_SET)
    transform_dataset(SUBMISSION_SET, TRANSFORMED_SUBMISSION_SET)
