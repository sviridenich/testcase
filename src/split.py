import pandas as pd
import numpy as np
from settings import *
import shutil

NUMBER_OF_SPLITS = 3
SAMPLE_SHARES = [0.7, 0.2, 0.1]
RANDOM_SEED = 124578

if __name__=="__main__":

    df = pd.read_csv(TRANSFORMED_TRAIN_SET)

    np.random.seed(RANDOM_SEED)
    split_mask = np.random.choice(NUMBER_OF_SPLITS, df.shape[0], p=SAMPLE_SHARES)

    df[split_mask==0].to_csv(TRAIN_SUBSET)
    df[split_mask==1].to_csv(VALIDATION_SUBSET)
    df[split_mask==2].to_csv(TEST_SUBSET)

    shutil.copy(src=TRANSFORMED_SUBMISSION_SET, dst=SUBMISSION_SUBSET)