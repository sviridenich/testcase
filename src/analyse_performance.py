from settings import PERFORMANCE_REPORT
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

if __name__=="__main__":
    df = pd.read_csv(PERFORMANCE_REPORT)
    sns.set()
    ax = sns.heatmap(  df.pivot(index = 'neurons', columns='epoc')['auc'], annot=True, fmt='.1%')
    plt.show()
    # can be viewed and saved manually into corresponding directory
    # the winner is the model that has 32 neurong and was trained for 1024 epochs
    # performance (auroc) on validation dataset is 86.4%