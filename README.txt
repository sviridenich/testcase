
The purpose of this project is to implement scoring
Data folder contains original provided data as well as transformed/preprocessed data
requirements.txt the list of modules installed on top of bare python
configuration for virtual environment can be found at corressponding folder - venv

In short, I have used the following approach to implement the scoring
 - conduct quick EDA - to understand how the data looks like, where we might have issues such as missing values
 - do the basic transformation , where i have introduced dummy variable for missing values in original features
 - fit neural network algorithm
 - find the most best hyper parameters for artificial network

The main enablers for the described approach is:
 - distribution similarity between train and submission set, which will prevent us from overffitting
 - time variable is absent, therefore I do not need to check score stability over time
 - it is acceptable to build black box algo